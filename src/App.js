import React from 'react';
import './App.css';
import { Switch, Route, Redirect, Link} from 'react-router-dom'

class App extends React.Component {
  render(){
    return (
      <div className='App'>
        <Header />
        <Main />
      </div>
    );
  }
}

function Header(){
  return(
    <header>
      <nav>
        <ul>
          <li><Link to='/red'>Red</Link></li>
          <li><Link to='/yellow'>Yellow</Link></li>
          <li><Link to='/green'>Green</Link></li>
        </ul>
      </nav>
    </header>
  )
}


const Main = () => (
  <main>
    <Switch>
      <div className='traffic'>
      <Route exact path='/' component={() => <Redirect to='/red' />} />
        <Route exact path='/red' component={Red} />
        <Route exact path='/yellow' component={Yellow}/>
        <Route path='/green' component={Green}/>
      </div>
    </Switch>
  </main>
)

const Red = () => (
  <svg>
    <circle r='25px' cx='30px' cy='30px' stroke='black' fill='red'/>
    <circle r='25px' cx='30px' cy='80px' stroke='black' fill='yellow' opacity='0.2'/>
    <circle r='25px' cx='30px' cy='130px' stroke='black' fill='lightgreen' opacity='0.2'/>
  </svg>
)

const Yellow = () => (
  <svg>
    <circle r='25px' cx='30px' cy='30px' stroke='black' fill='red' opacity='0.2'/>
    <circle r='25px' cx='30px' cy='80px' stroke='black' fill='yellow' />
    <circle r='25px' cx='30px' cy='130px' stroke='black' fill='lightgreen' opacity='0.2'/>
  </svg>
)

const Green = () => (
  <svg>
    <circle r='25px' cx='30px' cy='30px' stroke='black' fill='red' opacity='0.2'/>
    <circle r='25px' cx='30px' cy='80px' stroke='black' fill='yellow' opacity='0.2'/>
    <circle r='25px' cx='30px' cy='130px' stroke='black' fill='lightgreen' />
  </svg>
)

export default App;